package com.example;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;

import java.sql.*;
import java.util.Arrays;

public class DockerTest {
    private static final Logger logger = LoggerFactory.getLogger(DockerTest.class);

    public static void sampleInitFunction(Connection connection) throws SQLException {
        System.out.println(connection);
        String sql = "CREATE TABLE `games` (\n" +
                "  `id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор игры',\n" +
                "  `name` varchar(255) DEFAULT NULL COMMENT 'Программное имя игры (enum)',\n" +
                "  `title` varchar(255) DEFAULT NULL COMMENT 'Наименование игры',\n" +
                "  `lotos_id` smallint(6) DEFAULT NULL COMMENT 'Внешний идентификатор игры',\n" +
                "  `abbr` varchar(255) DEFAULT NULL COMMENT 'Аббревиатура, как в HGS/DDS',\n" +
                "  PRIMARY KEY (`id`),\n" +
                "  KEY `abbr` (`abbr`)\n" +
                ") ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;\n";
        Statement statement = connection.createStatement();
        statement.execute(sql);
    }


    @Test
    public void JDBC() throws SQLException {

        Connection connection = DriverManager.getConnection("jdbc:tc:mariadb:10.3.6://localhost:3306/testbase?TC_INITFUNCTION=ru.ushakov.dockerTest.com.example.DockerTest::sampleInitFunction", "", "");
        System.out.println("Creating database...");
        Statement statement = connection.createStatement();

        String sql = "SHOW TABLES";
        ResultSet resultSet = statement.executeQuery(sql);
        System.out.println(resultSet);
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        int columnCount = resultSetMetaData.getColumnCount();

        while (resultSet.next()) {
            Object[] values = new Object[columnCount];
            for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
                values[i - 1] = resultSet.getObject(i);
            }
            System.out.println(Arrays.asList(values));
        }
        System.out.println("Database created successfully...");
    }

    @Test
    public void blabla() {
        try (MariaDBContainer mysql = (MariaDBContainer) new MariaDBContainer().withLogConsumer(new Slf4jLogConsumer(logger))) {

            mysql.start();
            try (ResultSet resultSet = performQuery(mysql, "SELECT 1")) {
                int resultSetInt = resultSet.getInt(1);

                Assert.assertEquals("A basic SELECT query succeeds", 1, resultSetInt);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                mysql.stop();
            }
        }
    }

    @NonNull
    protected ResultSet performQuery(MariaDBContainer containerRule, String sql) throws SQLException {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(containerRule.getJdbcUrl());
        hikariConfig.setUsername(containerRule.getUsername());
        hikariConfig.setPassword(containerRule.getPassword());

        try (HikariDataSource ds = new HikariDataSource(hikariConfig);
             Statement statement = ds.getConnection().createStatement();
             ResultSet resultSet = statement.getResultSet()) {

            statement.execute(sql);
            resultSet.next();

            return resultSet;
        }
    }

}

